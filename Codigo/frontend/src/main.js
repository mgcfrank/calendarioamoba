import { createApp } from 'vue'
import { createPinia } from 'pinia'
import router from './router'

import './styles/tailwind.css';
import './styles/app.css';

//popup de informacion
import VueTippy from 'vue-tippy'
import 'tippy.js/dist/tippy.css' // optional for styling
// componente de icono
import MdIcon from './components/md-icon.vue';
// componente de modal
import ModalBase from '@/components/ModalBase.vue';
//ruta principal
import App from './App.vue'
// import Idialog from '@/composables/IdialogV2.js';

const app = createApp(App)
app.component("md-icon", MdIcon);
app.component("ModalBase", ModalBase);
app.use(createPinia())
app.use(router)
app.use(VueTippy)

app.mount('#app')
