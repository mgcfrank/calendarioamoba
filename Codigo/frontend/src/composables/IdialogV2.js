//border-4 border-`+color+` border-opacity-60
var idialog = {
	loading: {
		show: function ({ title = "Espere porfavor...", message = "Cargando datos", color = "primary" } = {}) {
			const html = `
     <div class="idialog-loading bg-black bg-opacity-30 flex flex-col w-full h-screen fixed z-50 top-0 left-0 p-4 items-center justify-center">
       <div class="bg-white rounded-md w-full sm:w-2/5 md:w-96 lg:w-96 fade-in-down flex flex-col justify-center items-center p-2">
          <svg class="animate-spin  h-10 w-10 text-`+ color + `" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
            <circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle>
            <path class="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
          </svg>
          <h1 class="text-`+ color + ` font-bold text-center">${title}</h1>
          <h1 class="text-`+ color + ` text-sm text-center">${message}</h1>
       </div>
     </div>`;
			//con js agregamos el html al dom de la pagina principal
			document.body.insertAdjacentHTML('beforeend', html);
		},
		clear: function () {
			if (document.getElementsByClassName('idialog-loading').length) {
				let elementoLoading = document.body.querySelector('.idialog-loading')
				document.body.removeChild(elementoLoading);
			}
		}
	},
	loadingToast: {
		show: function ({ title = "Espere porfavor...", message = "Cargando datos", color = "primary" } = {}) {
			const html = `
     <div class="idialog-loading-toast flex flex-col w-full fixed z-50 top-2/4 left-1/2 right-2/4 p-4 items-center" style="transform: translate(-50%, -50%)">
       <div class="bg-white rounded-md w-full sm:w-2/5 md:w-96 lg:w-96 fade-in-down flex flex-col justify-center items-center p-2">
          <svg class="animate-spin  h-10 w-10 text-`+ color + `" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
            <circle class="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle>
            <path class="opacity-75" fill="currentColor" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
          </svg>
          <h1 class="text-`+ color + ` font-bold text-center">${title}</h1>
          <h1 class="text-`+ color + ` text-sm text-center">${message}</h1>
       </div>
     </div>`;
			//con js agregamos el html al dom de la pagina principal
			document.body.insertAdjacentHTML('beforeend', html);
		},
		clear: function () {
			if (document.getElementsByClassName('idialog-loading-toast').length) {
				let elementoLoading = document.body.querySelector('.idialog-loading-toast')
				document.body.removeChild(elementoLoading);
			}
		}
	},

	confirm: {
		show: function ({
			title = "Eliminar registro",
			message = "¿Realmente quieres eliminar este registo? Este proceso no se puede revertir.",
			color = "primary",
			textCancel = 'No, cancelar!',
			textSuccess = '¡Sí, confírmado!'
		} = {}) {
			return new Promise(function (resolve, reject) {
				//<i class="mdi mdi-delete text-2xl text-`+ color +`"></i>
				const html = `
        <div
          class="idialog-confirm fade-in bg-black bg-opacity-40 flex items-center justify-center overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 w-full inset-0 h-full">
          <div class="fade-in-down w-full max-w-md p-5 relative mx-auto my-auto rounded-xl shadow-lg bg-white">
            <div class="text-center p-2 flex-auto justify-center">

              <h2 class="text-xl font-bold py-4">${title}</h2>
              <p class="text-md text-gray-600 px-8">${message}</p>
            </div>
            <div class="p-3 mt-2 text-center space-x-4 md:block">

              <button
                class="idialog-confirm-close mb-2 md:mb-0 bg-white px-5 py-2 text-sm shadow-sm font-medium tracking-wide border text-gray-600 rounded-md hover:shadow-lg hover:bg-gray-100">
                ${textCancel}
              </button>
              <button
                class="idialog-confirm-success mb-2 md:mb-0 bg-`+ color + ` border border-` + color + ` px-5 py-2 text-sm shadow-sm font-medium tracking-wide text-white rounded-md hover:shadow-lg hover:bg-` + color + `">
                ${textSuccess}
              </button>
            </div>
          </div>
        </div>`;

				//agregamos el html al dom de la pagina
				document.body.insertAdjacentHTML('beforeend', html);

				// agregamos el vento click en el boton de aceptar
				var ok_button = document.querySelector('.idialog-confirm-success');
				ok_button.addEventListener('click', function (event) {
					//retornamos un true a la promesa
					resolve(true);
					if (document.getElementsByClassName('idialog-confirm').length) {
						const elementoLoading = document.body.querySelector('.idialog-confirm')
						document.body.removeChild(elementoLoading);
					}
				}
				);
				//agregamos el vento click en el boton de cerrar
				var close_button = document.querySelector('.idialog-confirm-close');
				close_button.addEventListener('click', function (event) {
					//retornamos un false a la promesa
					resolve(false);
					if (document.getElementsByClassName('idialog-confirm').length) {
						const elementoLoading = document.body.querySelector('.idialog-confirm')
						document.body.removeChild(elementoLoading);
					}
				}
				);


			});
		}
	},

	toast: {
		show: function ({ title = "Espere porfavor...", message = "", icon = null, color = "primary", time = 2000 } = {}) {
			const html = `<div class="idialog-toast fade-in-right z-50 bg-error  border-${color} bg-opacity-80 hover:bg-opacity-95 border flex w-full sm:w-1/2 md:w-64 py-4 px-6 top-2 right-2 absolute text-white rounded-lg shadow-lg items-center">
       ${icon != null ?
					`<div class="bg-white rounded-full w-7 h-7 mr-3 pt-1 flex justify-center items-center">
         <md-icon icon="plus" class="font-bold text-2xl text-${color}"/>
        </div>`: ''}
       <div class="flex-1">
         <h1 class="text-sm">${title}</h1>
         <h2 class="text-xs">${message}</h2>
       </div>
     </div>`
			//con js agregamos el html al dom de la pagina principal
			document.body.insertAdjacentHTML('beforeend', html);

			//agregamos un cronometro
			let close_timeout = null;
			close_timeout = window.setTimeout(function (e) {
				// Clear timeout and interval
				clearTimeout(e);
				// cerramos el toast
				if (document.getElementsByClassName('idialog-toast').length) {
					const elementoLoading = document.body.querySelector('.idialog-toast')
					document.body.removeChild(elementoLoading);
				}
			}, time);

			let box = document.body.querySelector('.idialog-toast')
			window.onload = box.addEventListener(
				'mouseenter',
				function (event) {
					// Prevent default click behaviour
					event.preventDefault();
					console.log("en foco");
					// Clear timeout and interval
					clearTimeout(close_timeout);
				},
				false
			);

			window.onload = box.addEventListener(
				'mouseleave',
				function (event) {
					// Prevent default click behaviour
					event.preventDefault();
					// Self destruct after some time
					close_timeout = window.setTimeout(function () {
						// Clear timeout and interval
						clearTimeout(close_timeout);
						// Close the box
						// cerramos el toast
						if (document.getElementsByClassName('idialog-toast').length) {
							const elementoLoading = document.body.querySelector('.idialog-toast')
							document.body.removeChild(elementoLoading);
						}
					}, time / 2);
				},
				false
			);

		},
		clear: function () {
			if (document.getElementsByClassName('idialog-toast').length) {
				const elementoLoading = document.body.querySelector('.idialog-toast')
				document.body.removeChild(elementoLoading);
			}
		}
	},

};

export default idialog;
