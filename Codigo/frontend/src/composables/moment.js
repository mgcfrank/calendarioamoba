// manejo de fechas
import moment from 'moment'
export default {
	get(date = "") {
		return moment(date != '' ? date : new Date()).format('YYYY-MM-DD HH:mm:ss')
	},
	getDate(date = "") {
		return moment(date != '' ? date : new Date()).format('YYYY-MM-DD')
	},
	getTime(date = "") {
		return moment(date != '' ? date : new Date()).format('HH:mm:ss')
	}
}
