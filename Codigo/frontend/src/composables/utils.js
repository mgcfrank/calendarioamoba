/*eslint no-useless-escape: "error"*/
const isEmpty = (obj) => {
	if (obj && !Array.isArray(obj)) {
		return (Object.keys(obj).length > 0) ? false : true;
	} else if (obj && Array.isArray(obj)) {
		return obj.length > 0 ? false : true;
	} else if (obj) {
		return obj != null || obj.length > 0 ? false : true;
	} else {
		return true
	}
}
const isNumber = (numero) => {
	return /^-?\d+$/.test(numero);
	// return numero == '' ? false : !Number(numero) ? false : true;
	// console.log({}, utils.isNumber({}));
	// console.log([], utils.isNumber([]));
	// console.log('aa', utils.isNumber('aa'));
	// console.log('', utils.isNumber(''));
	// console.log('0', utils.isNumber('0'));
	// console.log('3', utils.isNumber('3'));
	// console.log(0, utils.isNumber(0));
	// console.log(3, utils.isNumber(3));
}
export default {
	isEmpty,
	isNotEmpty(obj) {
		return !isEmpty(obj);
	},
	isNumber,
	deepCopy(obj) {
		if (null == obj || "object" != typeof obj || obj instanceof RegExp) return obj;
		if (obj instanceof Date) {
			var copy = new Date();
			copy.setTime(obj.getTime());
			return copy;
		}
		if (obj instanceof Array) {
			var copy = [];
			for (var i = 0, len = obj.length; i < len; i++) {
				copy[i] = deepCopy(obj[i]);
			}
			return copy;
		}
		if (obj instanceof Object) {
			var copy = {};
			for (var attr in obj) {
				if (obj.hasOwnProperty(attr)) copy[attr] = deepCopy(obj[attr]);
			}
			return copy;
		}
	},

	generateUUID() {
		var dt = new Date().getTime();
		var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
			var r = (dt + Math.random() * 16) % 16 | 0;
			dt = Math.floor(dt / 16);
			return (c == 'x' ? r : (r & 0x3) | 0x8).toString(16);
		});
		return uuid;
	},
	trimmedString(string, length = 20) {
		if (string != null) {
			var length = 20;
			return string.length > length ? string.substring(0, length - 3) + "..." : string;
		}
		return "...";
	},
	isItAnEmail(valor) {
		//valida si es un correo
		if (
			/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(
				valor
			)
		) {
			return true;
		} else {
			return false;
		}
	},
	round(num, decimal = 2) {
		return num ? parseFloat(num).toFixed(decimal) : 0;
	},
	isItANumber(valor) {
		//valida si es un numero
		if (valor.match(/^[0-9]+$/)) {
			return true;
		} else {
			return false;
		}
	},
	isItANumberDecimal(valor) {
		//valida si es un numero decimal
		let valoresAceptados = /^[0-9]+$/;
		if (valor.indexOf('.') === -1) {
			if (valor.match(valoresAceptados)) {
				return true;
			} else {
				return false;
			}
		} else {
			//dividir la expresión por el punto en un array
			var particion = valor.split('.');
			//evaluamos la primera parte de la división (parte entera)
			if (particion[0].match(valoresAceptados) || particion[0] == '') {
				if (particion[1].match(valoresAceptados)) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
	},
	random(min, max) {
		return Math.floor(Math.random() * (max - min + 1) + min);
	},
	roundNumber(numb, decimal) {
		return numb.toFixed(decimal);
	},
	zeroFill(number=0, width =6) {
		var numberOutput = Math.abs(number); /* Valor absoluto del número */
		var length = number.toString().length; /* Largo del número */
		var zero = "0"; /* String de cero */

		if (width <= length) {
			if (number < 0) {
				return ("-" + numberOutput.toString());
			} else {
				return numberOutput.toString();
			}
		} else {
			if (number < 0) {
				return ("-" + (zero.repeat(width - length)) + numberOutput.toString());
			} else {
				return ((zero.repeat(width - length)) + numberOutput.toString());
			}
		}
		// return "0".repeat(len - number.toString().length) + number.toString();
	},
	formatPrice(value) {
		let val = (value / 1).toFixed(2).replace(',', '.')
		return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
		// return (value / 1).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
		// return Intl.NumberFormat('en-US').format(parseFloat((value / 1).toFixed(2)))
	},
	textNoEmpty(value) {
		//si un texto esta vacio muestra un contenido para que no estae vacio
		return value == null || value == 'null' || value.length <= 0 ? '---' : value;
	},
	existConnetion() {
		var cadena = 'http://google.com';

		var request = new XMLHttpRequest();
		return request.open('GET', cadena, false);

		if (request.status == '200') {
			return true;
		}
		return false;
	},
	getDateSystem() {
		var date = new Date();
		return (
			date.getFullYear() +
			'-' +
			(date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) +
			'-' +
			(date.getDate() < 10 ? '0' + date.getDate() : date.getDate())
		);
	},
	isObjEmpty(obj) {
		//validar si un objeto esta vacio
		for (var prop in obj) {
			if (obj.hasOwnProperty(prop)) return false;
		}
		return true;
	},
	countObject(obj) {
		let lengthOfObject = Object.keys(obj).length;
		return lengthOfObject;
	},
	isKeyExists(obj, key) {
		if (obj[key] == undefined) {
			return false;
		} else {
			return true;
		}
	},
	parseErrorHttp(error) {
		var itemsError = '';
		if (error.response) {
			console.log("HTTP ERROR: RESPONSE", error.response);
			//verificamos que el token no haya caducado
			if (error.response.status == 401) {
				localStorage.clear();
				window.location.href = "/login";
				return;
			}
			if (error.response.status == 422 || error.response.status == 400) {
				//recuperamos el error del backend
				const errors = error.response.data;
				//iteramos en cada error para mostrarlo
				errors.forEach((item) => {
					itemsError += item + '<br>';
				});
				// //recuperamos el error del backend
				// const errors = error.response.data.errors;
				// //recuperamos la keys del objeto
				// const keys = Object.keys(errors);
				// //iteramos en cada error para mostrarlo
				// keys.forEach((key, index) => {
				//   console.log(`${key}: ${errors[key]}`);
				//   errors[key].forEach(error => {
				//     itemsError += error + '\n';
				//   });
				// });
			} else {
				itemsError =
					(error.response && error.response.data && error.response.data.message) ||
					error.message ||
					error.toString();
			}
		} else if (error.request) {
			console.log("HTTP ERROR: REQUEST", error.request);
			// The request was made but no response was received
			// Se realizó la solicitud pero no se recibió respuesta
			itemsError = error.request.toString()
		} else {
			console.log("HTTP ERROR: CATCH CLIENT", error);
			// Something happened in setting up the request that triggered an Error
			// Algo sucedió al configurar la solicitud que provocó un error
			itemsError = error.message;
		}
		return itemsError;
	},
};
