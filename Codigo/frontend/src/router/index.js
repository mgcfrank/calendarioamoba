import { createRouter, createWebHistory, createWebHashHistory } from "vue-router";
import { publicRoute, protectedRoute } from "./routes";

const routes = publicRoute.concat(protectedRoute);

const router = createRouter({
	history: createWebHashHistory(),
	linkActiveClass: "active",
	routes: routes,
});

router.beforeEach((to, from, next) => {
	//ponermos el titulo a la pagina
	document.title = to.name;
	const publicPages = ["/home"];
	const authRequired = !publicPages.includes(to.path);
	const authStore = localStorage.getItem('token');

	if (authRequired && !authStore) {
		// return "/login";
		next({ name: 'Home' });
	}
	next();
});

export default router;
