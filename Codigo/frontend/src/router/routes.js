import homeView from '@/views/Home.vue';

export const publicRoute = [
	{
		path: "/",
		component: () => import("@/layouts/Base/index.vue"),//define el layout principal
		children: [
			{
				path: "/",
				name: "Home",
				meta: { title: 'Home' },
				component: homeView,
			}
		]
	}
];
export const protectedRoute = [

];
