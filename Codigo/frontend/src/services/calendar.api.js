import httpClient from './httpClient';

const END_POINT = '/calendars';

const getCalendar = (params = {}) => httpClient.get(END_POINT, { params: params });
// const createCalendar = (data) => httpClient.post(END_POINT, data);
// const updateCalendar = (data) => httpClient.put(END_POINT, data);
// const deleteCalendar = (data) => httpClient.delete(`${END_POINT}/${data}`);

export {
	getCalendar,
	// createCalendar,
	// updateCalendar,
	// deleteCalendar
}
