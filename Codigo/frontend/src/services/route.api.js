import httpClient from './httpClient';

const END_POINT = '/routes';

const getRoute = (params = {}) => httpClient.get(END_POINT, { params: params });
// const createRoute = (data) => httpClient.post(END_POINT, data);
// const updateRoute = (data) => httpClient.put(END_POINT, data);
// const deleteRoute = (data) => httpClient.delete(`${END_POINT}/${data}`);

export {
	getRoute,
	// createRoute,
	// updateRoute,
	// deleteRoute
}
