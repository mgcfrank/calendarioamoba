// import Vue from 'vue'
import axios from 'axios';

//configuramos la url base del api
const httpClient = axios.create({
	baseURL: import.meta.env.VITE_APP_API_URL,
});

//actualizamos el header en caso de tener una autenticacion
const getAuthToken = () => localStorage.getItem('token');
httpClient.interceptors.request.use(
	(config) => {
		config.headers['Authorization'] = getAuthToken();
		return config;
	}
);

// response interceptor de respuestsa al cliente
httpClient.interceptors.response.use(
	(response) => {
		//envia respuestas recibidas correctas
		console.log(response);
		// GLOBAL.log(response);
		return response;
	},
	(error) => {
		console.log(error);
		// GLOBAL.log(error);
		if (error.response) {
			//verificamos que el token no haya caducado
			if (error.response.status == 401) {
				localStorage.removeItem('token');
				// router.push('/auth');
			} else {
				//cualquier error 400
			}
		} else if (error.request) {
			// client never received a response, or request never left
			alert('No hay respuesta del servidor: \n\n' + error.request);
		} else {
			// anything else
			alert('No hay conexion con el servidor: \n\n' + error);
		}
		//envia respuestas fallidas 500 o 400
		return Promise.reject(error);
	}
);

export default httpClient;
