<?php

use App\Models\Calendar;
use App\Models\CalendarDaysDisabled;
use App\Models\Reservation;
use App\Models\Route;
use App\Models\RouteData;
use App\Models\Service;
use App\Models\User;
use App\Models\UserPlan;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $json  = file_get_contents(storage_path() . "/AMOBA/users.json");
        $users = json_decode($json);
        foreach ($users->users as $value) {
            $user = User::restoreRows($value);
            $user->save();
        }

        $json  = file_get_contents(storage_path() . "/AMOBA/user_plans.json");
        $user_plans = json_decode($json);
        foreach ($user_plans->user_plans as $value) {
            $user = UserPlan::restoreRows($value);
            $user->save();
        }

        $json  = file_get_contents(storage_path() . "/AMOBA/routes.json");
        $routes = json_decode($json);
        foreach ($routes->routes as $value) {
            $user = Route::restoreRows($value);
            $user->save();
        }

        $json  = file_get_contents(storage_path() . "/AMOBA/route_data.json");
        $routes_data = json_decode($json);
        foreach ($routes_data->routes_data as $value) {
            $user = RouteData::restoreRows($value);
            $user->save();
        }

        $json  = file_get_contents(storage_path() . "/AMOBA/calendar.json");
        $data = json_decode($json);
        foreach ($data->calendar as $value) {
            $user = Calendar::restoreRows($value);
            $user->save();
        }

        $json  = file_get_contents(storage_path() . "/AMOBA/calendar_days_disabled.json");
        $data = json_decode($json);
        foreach ($data->calendar_days_disabled as $value) {
            $user = CalendarDaysDisabled::restoreRows($value);
            $user->save();
        }

        $json  = file_get_contents(storage_path() . "/AMOBA/reservations.json");
        $data = json_decode($json);
        foreach ($data->reservations as $value) {
            $user = Reservation::restoreRows($value);
            $user->save();
        }

        $json  = file_get_contents(storage_path() . "/AMOBA/services.json");
        $data = json_decode($json);
        foreach ($data->services as $value) {
            $user = Service::restoreRows($value);
            $user->save();
        }
    }
}
