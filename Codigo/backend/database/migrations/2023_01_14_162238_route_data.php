<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RouteData extends Migration
{
    public function up()
    {
        Schema::create('route_data', function (Blueprint $table) {
            $table->id();
            $table->foreignId('route_id')->nullable()->constrained('routes');
            $table->integer('calendar_id')->nullable();
            $table->string('vinculation_route')->nullable();
            $table->integer('route_circular')->nullable();
            $table->timestamp('date_init')->nullable();
            $table->timestamp('date_finish')->nullable();
            $table->integer('mon')->nullable();
            $table->integer('tue')->nullable();
            $table->integer('wed')->nullable();
            $table->integer('thu')->nullable();
            $table->integer('fri')->nullable();
            $table->integer('sat')->nullable();
            $table->integer('sun')->nullable();

            $table->timestamp('updated_at')->nullable();
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('route_data');
    }
}
