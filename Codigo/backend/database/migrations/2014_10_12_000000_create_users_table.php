<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('picture')->nullable();
            $table->string('email')->unique();
            $table->string('password', 250)->nullable();
            $table->string('remember_token')->nullable();
            $table->timestamp('last_online')->nullable();
            $table->string('verification_code')->nullable();
            $table->string('new_email')->nullable();
            $table->integer('status')->nullable()->default(1);
            $table->integer('first')->nullable()->default(0);
            $table->timestamp('last_accept_date')->nullable();
            $table->string('company_contact')->nullable();
            $table->decimal('credits', 14, 2)->nullable();
            $table->integer('first_trip')->nullable();
            $table->integer('incomplete_profile')->nullable();
            $table->integer('phone_verify')->nullable();
            $table->string('token_auto_login')->nullable();
            $table->string('user_vertical')->nullable();
            $table->integer('language_id')->nullable()->default(0);
            $table->integer('no_registered')->nullable()->default(0);

            $table->timestamp('created')->nullable();
            $table->timestamp('modified')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
