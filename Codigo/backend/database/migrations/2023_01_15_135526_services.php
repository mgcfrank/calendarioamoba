<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Services extends Migration
{
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('external_id')->nullable();
            $table->string('external_budget_id')->nullable();
            $table->string('external_route_id')->nullable();
            $table->integer('track_id')->nullable();
            $table->string('name')->nullable();
            $table->string('notes')->nullable();
            $table->timestamp('timestamp')->nullable();
            $table->string('arrival_address')->nullable();
            $table->timestamp('arrival_timestamp')->nullable();
            $table->string('departure_address')->nullable();
            $table->timestamp('departure_timestamp')->nullable();

            $table->integer('capacity')->nullable();
            $table->integer('confirmed_pax_count')->nullable();
            $table->timestamp('reported_departure_timestamp')->nullable();
            $table->string('reported_departure_kms')->nullable();
            $table->timestamp('reported_arrival_timestamp')->nullable();
            $table->string('reported_arrival_kms')->nullable();
            $table->integer('reported_vehicle_plate_number')->nullable();
            $table->integer('status')->nullable();
            $table->text('status_info')->nullable();
            $table->integer('reprocess_status')->nullable();
            $table->integer('return')->nullable();

            $table->timestamp('created')->nullable();
            $table->timestamp('modified')->nullable();
            $table->string('synchronized_downstream')->nullable();
            $table->string('synchronized_upstream')->nullable();
            $table->integer('province_id')->nullable();
            $table->integer('sale_tickets_drivers')->nullable();
            $table->string('notes_drivers')->nullable();
            $table->text('itinerary_drivers')->nullable();
            $table->integer('cost_if_externalized')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
