<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserPlans extends Migration
{
    public function up()
    {
        Schema::create('user_plans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable()->constrained('users');
            $table->integer('currency_id')->nullable();
            $table->integer('next_user_plan_id')->nullable();
            $table->timestamp('start_timestamp')->nullable();
            $table->timestamp('end_timestamp')->nullable();
            $table->timestamp('renewal_timestamp')->nullable();
            $table->decimal('renewal_price', 14, 2)->nullable();
            $table->integer('requires_invoice')->nullable();
            $table->integer('status')->nullable();
            $table->integer('financiation')->nullable();
            $table->integer('status_financiation')->nullable();
            $table->string('language')->nullable();
            $table->string('nif')->nullable();
            $table->string('business_name')->nullable();
            $table->integer('pending_payment')->nullable();
            $table->timestamp('date_max_payment')->nullable();
            $table->timestamp('proxim_start_timestamp')->nullable();
            $table->timestamp('proxim_end_timestamp')->nullable();
            $table->timestamp('proxim_renewal_timestamp')->nullable();
            $table->timestamp('proxim_renewal_price')->nullable();
            $table->decimal('credits_return', 14, 2)->nullable();
            $table->integer('company_id')->nullable();
            $table->integer('cancel_employee')->nullable();
            $table->integer('force_renovation')->nullable();
            $table->timestamp('date_canceled')->nullable();
            $table->decimal('amount_confirm_canceled', 14, 2)->nullable();
            $table->decimal('credit_confirm_canceled', 14, 2)->nullable();
            $table->integer('cost_center_id')->nullable();
            $table->timestamp('created')->nullable();
            $table->timestamp('modified')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_plans');
    }
}
