<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CalendarDaysDisableds extends Migration
{
    public function up()
    {
        Schema::create('calendar_days_disableds', function (Blueprint $table) {
            $table->id();
            $table->foreignId('calendar_id')->nullable()->constrained('calendars');
            $table->timestamp('day')->nullable();
            $table->integer('enabled')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendar_days_disableds');
    }
}
