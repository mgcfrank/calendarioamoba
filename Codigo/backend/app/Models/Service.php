<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Service extends Authenticatable
{
    use Notifiable;
    protected $guarded = [];
    public $timestamps = false;

    public static function restoreRows($oldValue)
    {
        $row = new static();

        if ($oldValue) {
            $row->id = $oldValue->id;
            $row->external_id = $oldValue->external_id;
            $row->external_budget_id = $oldValue->external_budget_id;
            $row->external_route_id = $oldValue->external_route_id;
            $row->track_id = $oldValue->track_id;
            $row->name = $oldValue->name;
            $row->notes = $oldValue->notes;
            $row->timestamp = $oldValue->timestamp;
            $row->arrival_address = $oldValue->arrival_address;
            $row->arrival_timestamp = $oldValue->arrival_timestamp;
            $row->departure_address = $oldValue->departure_address;
            $row->departure_timestamp = $oldValue->departure_timestamp;
            $row->capacity = $oldValue->capacity;
            $row->confirmed_pax_count = $oldValue->confirmed_pax_count;
            $row->reported_departure_timestamp = $oldValue->reported_departure_timestamp;
            $row->reported_departure_kms = $oldValue->reported_departure_kms;
            $row->reported_arrival_timestamp = $oldValue->reported_arrival_timestamp;
            $row->reported_arrival_kms = $oldValue->reported_arrival_kms;
            $row->reported_vehicle_plate_number = $oldValue->reported_vehicle_plate_number;
            $row->status = $oldValue->status;
            $row->status_info = $oldValue->status_info;
            $row->reprocess_status = $oldValue->reprocess_status;
            $row->return = $oldValue->return;
            $row->created = $oldValue->created;
            $row->modified = $oldValue->modified;
            $row->synchronized_downstream = $oldValue->synchronized_downstream;
            $row->synchronized_upstream = $oldValue->synchronized_upstream;
            $row->province_id = $oldValue->province_id;
            $row->sale_tickets_drivers = $oldValue->sale_tickets_drivers;
            $row->notes_drivers = $oldValue->notes_drivers;
            $row->itinerary_drivers = $oldValue->itinerary_drivers;
            $row->cost_if_externalized = $oldValue->cost_if_externalized;
        }

        return $row;
    }

    // public function Route()
    // {
    //     return $this->belongsTo(Route::class, 'route_id');
    // }
}
