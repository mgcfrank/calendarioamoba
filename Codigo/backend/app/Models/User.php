<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
    protected $guarded = [];
    public $timestamps = false;

    public static function restoreRows($oldValue)
    {
        $user = new static();

        if ($oldValue) {
            $user->id = $oldValue->id;
            $user->first_name = $oldValue->first_name;
            $user->last_name = $oldValue->last_name;
            $user->phone_number = $oldValue->phone_number;
            $user->picture = $oldValue->picture;
            $user->email = $oldValue->email;
            $user->password = $oldValue->password;
            $user->remember_token = $oldValue->remember_token;
            $user->last_online = $oldValue->last_online;
            $user->verification_code = $oldValue->verification_code;
            $user->new_email = $oldValue->new_email;
            $user->status = $oldValue->status;
            $user->first = $oldValue->first;
            $user->last_accept_date = $oldValue->last_accept_date;
            $user->created = $oldValue->created;
            $user->modified = $oldValue->modified;
            $user->company_contact = $oldValue->company_contact;
            $user->credits = $oldValue->credits;
            $user->first_trip = $oldValue->first_trip;
            $user->incomplete_profile = $oldValue->incomplete_profile;
            $user->phone_verify = $oldValue->phone_verify;
            $user->token_auto_login = $oldValue->token_auto_login;
            $user->user_vertical = $oldValue->user_vertical;
            $user->language_id = $oldValue->language_id;
            $user->no_registered = $oldValue->no_registered;
            $user->deleted_at = $oldValue->deleted_at;
        }

        return $user;
    }

    public function UserPlans()
    {
        return $this->hasMany(UserPlan::class, 'id');
    }
}
