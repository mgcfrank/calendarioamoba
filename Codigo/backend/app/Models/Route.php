<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Route extends Authenticatable
{
    use Notifiable;
    protected $guarded = [];
    public $timestamps = false;

    public static function restoreRows($oldValue)
    {
        $row = new static();

        if ($oldValue) {
            $row->id = $oldValue->id;
            $row->external_id = $oldValue->external_id;
            $row->invitation_code = $oldValue->invitation_code;
            $row->title = $oldValue->title;
            $row->start_timestamp = $oldValue->start_timestamp;
            $row->end_timestamp = $oldValue->end_timestamp;
        }

        return $row;
    }

    public function RouteDatas()
    {
        return $this->hasMany(RouteData::class, 'id');
    }
}
