<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class UserPlan extends Authenticatable
{
    use Notifiable;
    protected $guarded = [];
    public $timestamps = false;

    public static function restoreRows($oldValue)
    {
        $row = new static();

        if ($oldValue) {
            $row->id = $oldValue->id;
            $row->user_id = $oldValue->user_id;
            $row->currency_id = $oldValue->currency_id;
            $row->next_user_plan_id = $oldValue->next_user_plan_id;
            $row->start_timestamp = $oldValue->start_timestamp;
            $row->end_timestamp = $oldValue->end_timestamp;
            $row->renewal_timestamp = $oldValue->renewal_timestamp;
            $row->renewal_price = $oldValue->renewal_price;
            $row->requires_invoice = $oldValue->requires_invoice;
            $row->status = $oldValue->status;
            $row->created = $oldValue->created;
            $row->modified = $oldValue->modified;
            $row->financiation = $oldValue->financiation;
            $row->status_financiation = $oldValue->status_financiation;
            $row->language = $oldValue->language;
            $row->nif = $oldValue->nif;
            $row->business_name = $oldValue->business_name;
            $row->pending_payment = $oldValue->pending_payment;
            $row->date_max_payment = $oldValue->date_max_payment;
            $row->proxim_start_timestamp = $oldValue->proxim_start_timestamp;
            $row->proxim_end_timestamp = $oldValue->proxim_end_timestamp;
            $row->proxim_renewal_timestamp = $oldValue->proxim_renewal_timestamp;
            $row->proxim_renewal_price = $oldValue->proxim_renewal_price;
            $row->credits_return = $oldValue->credits_return;
            $row->company_id = $oldValue->company_id;
            $row->cancel_employee = $oldValue->cancel_employee;
            $row->force_renovation = $oldValue->force_renovation;
            $row->date_canceled = $oldValue->date_canceled;
            $row->amount_confirm_canceled = $oldValue->amount_confirm_canceled;
            $row->credit_confirm_canceled = $oldValue->credit_confirm_canceled;
            $row->cost_center_id = $oldValue->cost_center_id;
        }

        return $row;
    }

    public function Users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
