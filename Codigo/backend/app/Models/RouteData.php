<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class RouteData extends Authenticatable
{
    use Notifiable;
    protected $guarded = [];
    public $timestamps = false;

    public static function restoreRows($oldValue)
    {
        $row = new static();

        if ($oldValue) {
            $row->id = $oldValue->id;
            $row->route_id = $oldValue->route_id;
            $row->calendar_id = $oldValue->calendar_id;
            $row->vinculation_route = $oldValue->vinculation_route;
            $row->route_circular = $oldValue->route_circular;
            $row->date_init = $oldValue->date_init;
            $row->date_finish = $oldValue->date_finish;
            $row->mon = $oldValue->mon;
            $row->tue = $oldValue->tue;
            $row->wed = $oldValue->wed;
            $row->thu = $oldValue->thu;
            $row->fri = $oldValue->fri;
            $row->sat = $oldValue->sat;
            $row->sun = $oldValue->sun;
            $row->updated_at = $oldValue->updated_at;
            $row->created_at = $oldValue->created_at;
        }

        return $row;
    }

    public function Route()
    {
        return $this->belongsTo(Route::class, 'route_id');
    }
}
