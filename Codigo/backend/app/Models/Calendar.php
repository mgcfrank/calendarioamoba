<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Calendar extends Authenticatable
{
    use Notifiable;
    protected $guarded = [];
    public $timestamps = false;

    public static function restoreRows($oldValue)
    {
        $row = new static();

        if ($oldValue) {
            $row->id = $oldValue->id;
            $row->calendar_id = $oldValue->calendar_id;
            $row->name = $oldValue->name;
            $row->updated_at = $oldValue->updated_at;
            $row->created_at = $oldValue->created_at;
        }

        return $row;
    }

    public function CalendarDaysDisabled()
    {
        return $this->hasMany(CalendarDaysDisabled::class, 'id');
    }
}
