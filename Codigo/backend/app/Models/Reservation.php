<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Reservation extends Authenticatable
{
    use Notifiable;
    protected $guarded = [];
    public $timestamps = false;

    public static function restoreRows($oldValue)
    {
        $row = new static();

        if ($oldValue) {
            $row->id = $oldValue->id;
            $row->user_plan_id = $oldValue->user_plan_id;
            $row->route_id = $oldValue->route_id;
            $row->track_id = $oldValue->track_id;
            $row->reservation_start = $oldValue->reservation_start;
            $row->reservation_end = $oldValue->reservation_end;
            $row->route_stop_origin_id = $oldValue->route_stop_origin_id;
            $row->route_stop_destination_id = $oldValue->route_stop_destination_id;
            $row->created_at = $oldValue->created_at;
            $row->reservation_end = $oldValue->reservation_end;
            $row->deleted_at = $oldValue->deleted_at;
        }

        return $row;
    }

    public function UserPlan()
    {
        return $this->belongsTo(UserPlan::class, 'user_plan_id');
    }

    public function Route()
    {
        return $this->belongsTo(Route::class, 'route_id');
    }
}
