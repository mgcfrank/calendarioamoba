<?php

namespace App\Http\Controllers;

use App\Models\CalendarDaysDisabled;
use App\Models\Reservation;
use App\Models\Route;
use App\Models\RouteData;
use App\Models\User;
use App\Models\UserPlan;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class RouteController extends Controller
{
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date_first'  => 'nullable',
            'date_second'  => 'nullable',
        ]);

        $build = Route::where(function ($query) use ($request) {
                //tiene rangos de fecha
                if (isset($request->date_first) and isset($request->date_second)) {
                    $query->whereRaw("start_timestamp >='".$request->date_first."' and end_timestamp <= '".$request->date_second . "'");
                    return;
                }

                //tiene solo una fecha
                if (isset($request->date_first)) {
                    $query->whereRaw("DATE(start_timestamp) = '".$request->date_first. "'");
                }
            })->get();

        return response()->json($build, Response::HTTP_OK);
    }
}
