<?php

namespace App\Http\Controllers;

use App\Models\CalendarDaysDisabled;
use App\Models\Reservation;
use App\Models\RouteData;
use App\Models\User;
use App\Models\UserPlan;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index()
    {
        $users = Reservation::with(['Route', 'UserPlan'])->get();
        return response()->json(json_decode($users, true), Response::HTTP_OK);
    }

    public function ListaPlanes()
    {
        $users = User::all();
        return response()->json(json_decode($users, true), Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required',
            'email'  => 'required',
            'password'  => 'required'
        ]);

        //existe un error?
        if ($validator->fails()) {
            return response()->json($validator->errors()->all(), Response::HTTP_BAD_REQUEST);
        }

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->save();

        return response()->json($user, Response::HTTP_CREATED);
    }
}
