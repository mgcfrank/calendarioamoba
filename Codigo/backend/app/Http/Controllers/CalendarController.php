<?php

namespace App\Http\Controllers;

use App\Models\CalendarDaysDisabled;
use App\Models\Reservation;
use App\Models\RouteData;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class CalendarController extends Controller
{
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date_first'  => 'required',
            'date_second'  => 'nullable',
            'route_id' => 'required'
        ]);

        //existe un error?
        if ($validator->fails()) {
            return response()->json($validator->errors()->all(), Response::HTTP_BAD_REQUEST);
        }

        //generamos todos los dias
        $mesActual = getDate(strtotime($request->date_first));
        $diasPrimeraFecha = cal_days_in_month(CAL_GREGORIAN, $mesActual['mon'], $mesActual['year']);

        $mesAnterior = date("d-m-Y", strtotime($request->date_first . "- 1 month"));
        $mesAnterior = getDate(strtotime($mesAnterior));
        $diasAnteriorMes = cal_days_in_month(CAL_GREGORIAN, $mesAnterior['mon'], $mesAnterior['year']);

        $mesSiguiente = date("d-m-Y", strtotime($request->date_first . "+ 1 month"));
        $mesSiguiente = getDate(strtotime($mesSiguiente));

        // dia de la semana que inicia la primerafecha
        $diaSemanaInicio = date("w", strtotime($request->date_first));
        $diaSemanaInicio = $diaSemanaInicio > 0 ? $diaSemanaInicio : 7;


        //:::::generamos el calendario de 6 semanas
        //tipo de mes: 0 = mes anterior; 1=Mes actual; 2=Mes siguiente

        //creamos un objeto para almacenar valores del calendario y frecuencia
        $CalendarioDias = [
            'calendar' => [],
            'frequencies' => null
        ];
        //1RO los ultimos dias del mes pasado
        if ($diaSemanaInicio != 1) {
            $cont = $diaSemanaInicio - 2;
            do {
                $auxDia = (((int)$diasAnteriorMes) - $cont);
                $CalendarioDias['calendar'][] = [
                    'tipo_mes' => 0,
                    'dia' => $auxDia,
                    'mes' => $mesAnterior['mon'],
                    'year' => $mesAnterior['year'],
                    'fecha' =>  date("Y-m-d", strtotime($mesAnterior['year'] . "-" . $mesAnterior['mon'] . "-" . $auxDia)),
                    'day_disabled' => false,
                    'day_reservated' => null,
                    'day_service' => null,
                    'route_capacity' => null
                ];
                $cont--;
            } while ($cont >= 0);
        }

        //2DO fechas del mes actual
        for ($i = 1; $i < $diasPrimeraFecha + 1; $i++) {
            $CalendarioDias['calendar'][] = [
                'tipo_mes' => 1,
                'dia' => $i,
                'mes' => $mesActual['mon'],
                'year' => $mesActual['year'],
                'fecha' =>  date("Y-m-d", strtotime($mesActual['year'] . "-" . $mesActual['mon'] . "-" . $i)),
                'day_disabled' => false,
                'day_reservated' => null,
                'day_service' => null,
                'route_capacity' => null
            ];
        }

        //3RO fechas del mes siguiente
        $ultimoMesDia = 1;
        do {
            $CalendarioDias['calendar'][] = [
                'tipo_mes' => 2,
                'dia' => $ultimoMesDia,
                'mes' => $mesSiguiente['mon'],
                'year' => $mesSiguiente['year'],
                'fecha' =>  date("Y-m-d", strtotime($mesSiguiente['year'] . "-" . $mesSiguiente['mon'] . "-" . $ultimoMesDia)),
                'day_disabled'=>false,
                'day_reservated' => null,
                'day_service'=> null,
                'route_capacity'=> null
            ];
            $ultimoMesDia++;
        } while (count($CalendarioDias['calendar']) < 42);



        //obtenemos dias deshabilitados
        //obtenemos las fechas segun el rango a peticion
        $dayDisabled = CalendarDaysDisabled::where(function ($query) use ($request) {
                //tiene rangos de fecha?
                if (isset($request->date_first) and isset($request->date_second)) {
                    $query->whereBetween('day', [$request->date_first, $request->date_second]);
                    // $query->whereRaw("day >='" . $request->date_first . "' and day <= '" . $request->date_second . "'");
                }
                //tiene solo una fecha?
                if (isset($request->date_first) and !isset($request->date_second)) {
                    $query->whereRaw("DATE(day) = '" . $request->date_first . "'");
                }
            })->get();

        //unimos las fechas con las del calendario
        foreach ($dayDisabled as $valueDisable) {
            $CalendarioDias['calendar'] = array_map(function ($valueCalendar) use ($valueDisable) {
                if (date("Y-m-d", strtotime($valueDisable->day)) == $valueCalendar['fecha']) {
                    $valueCalendar['day_disabled'] = true;
                }
                return $valueCalendar;
            }, $CalendarioDias['calendar']);
        }

        //obtenemos reservaciones
        $dayReservation = Reservation::with(['UserPlan', 'Route'])->where(function ($query) use ($request) {
            //tiene rangos de fecha?
            if (isset($request->date_first) and isset($request->date_second)) {
                $query->whereRaw(
                    "(reservation_start >= ? AND reservation_end <= ?)",
                    [
                        $request->date_first,
                        $request->date_second
                    ]);
                // $query->whereDate('reservation_start', '>=', $request->date_first)->whereDate('reservation_end', '<=', $request->date_second);
            }
            //tiene solo una fecha?
            if (isset($request->date_first) and !isset($request->date_second)) {
                $query->whereRaw("DATE(reservation_start) = '" . $request->date_first . "'");
            }
        })->get();
        // return response()->json($dayReservation, Response::HTTP_OK);

        foreach ($dayReservation as $valueReservation) {
            $CalendarioDias['calendar'] = array_map(function ($valueCalendar) use ($valueReservation) {
                if ($valueCalendar['fecha']>=date("Y-m-d", strtotime($valueReservation->reservation_start))
                && $valueCalendar['fecha'] <=date("Y-m-d", strtotime($valueReservation->reservation_end))) {
                    $valueCalendar['day_reservated'] = $valueReservation;
                }
                return $valueCalendar;
            },  $CalendarioDias['calendar']);
        }

        //servicios por dia
        $dayServices = Service::where(function ($query) use ($request) {
            //tiene rangos de fecha?
            if (isset($request->date_first) and isset($request->date_second)) {
                $query->whereBetween('timestamp', [$request->date_first, $request->date_second]);
            }
            //tiene solo una fecha?
            if (isset($request->date_first) and !isset($request->date_second)) {
                $query->whereRaw("DATE(timestamp) = '" . $request->date_first . "'");
            }
        })->get();

        foreach ($dayServices as $valueService) {
            $CalendarioDias['calendar'] = array_map(function ($valueCalendar) use ($valueService) {
                if (date("Y-m-d", strtotime($valueService->timestamp)) == $valueCalendar['fecha']) {
                    $valueCalendar['day_service'] = $valueService;
                }
                return $valueCalendar;
            },  $CalendarioDias['calendar']);
        }

        //seleccionamos la frecuencia de la ruta
        if(isset($request->route_id)){
            $build = RouteData::with(['Route'])->where('route_id','=', $request->route_id)->get();
            $CalendarioDias['frequencies'] = $build;
        }

        return response()->json($CalendarioDias, Response::HTTP_OK);
    }
}
