<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UsersTest extends TestCase
{
    use RefreshDatabase; //refresca la base de datos

    /** @test */
    public function list_of_posst_can_be_retrieved()
    {
        $this->withoutExceptionHandling();

        factory(User::class, 3)->create(); //datos de prueba
        $response = $this->get('/api/users'); //llamamos al controlador
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            '*' => []
        ]);
    }

    /** @test */
    public function a_post_can_be_created()
    {
        $this->withoutExceptionHandling();

        $response = $this->post('/api/users', [
            'name' => 'Juan',
            'email' => 'juan@example.com',
            'password' => 'Qwerty'
        ]);

        // $reponse->assertOk();
        $response->assertStatus(Response::HTTP_CREATED);
        $this->assertCount(1, User::All());

        $user = User::first();
        $this->assertEquals($user->name, 'Juan');
        $this->assertEquals($user->email, 'juan@example.com');
        // $this->assertDatabaseHas('users', $payload);
    }
}
